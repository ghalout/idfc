from __future__ import unicode_literals
from PaxcelBotFramework.InputOutputManager.models import SimpleInputProcessor,OutputProcessorType1
from BotManager.models  import getTemplateBotResponse
from DecisionLogicLayer.decisionLogicLayer import processBotResponse
import json
from MessageFormationLayer.messageHandler import createMessage
import os
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from PaxcelBotFramework.DataAccessLayer.chatlogsHandler import dbChatlogHandler
from django.conf import settings
import json
import logging
from threading import Timer
logger = logging.getLogger(__name__)
from threading import Timer




@csrf_exempt
def processInputRequest(request):

    """

    This is the first function called after a request is made

    :param request: request object
    :return: an http response


    """

    ##
    ## Initialise the Input Processor Object
    ##
    print request
    if request.method == 'POST':
        requestProcessor = SimpleInputProcessor()
        requestProcessor.extractRequestData(request)
        InputRequest = requestProcessor.createBotRequest()
        responseType = requestProcessor.responseType
        responseObject = getTemplateBotResponse(InputRequest)
        decisionLogicResponse = processBotResponse(responseObject)
        userMessage = responseObject["outputMessages"][-1]["message"]

        # logger.info(
        #     "User Query: {0}".format((userMessage)),
        #     extra={'ConversationId': responseObject["botRequest"]["info"]['ConvSessionId']}
        # )
        messageLayerResponse = createMessage(decisionLogicResponse)
        outputDict = greenBeretBotOutputProcessorType1(outputContent=messageLayerResponse,
                                                  responseType=responseType).processRequest()

        infoField = responseObject["botRequest"]["info"]
        # processInputRequest.var = infoField
        # if "phoneNo" in infoField.keys():
        #     delay_in_sec = 60
        #
        #     t = Timer(delay_in_sec, hello, [
        #         delay_in_sec])  # hello function will be called 2 sec later with [delay_in_sec] as *args parameter
        #     t.start()

        if "convId" and "seqNo" and "chatLogsConvId" in infoField.keys():
           if "SimpleMessage" in outputDict.keys():
               botReply = (outputDict['SimpleMessage'])
               print "botReply : ", botReply
               for reply in range(len(botReply)):
                   text1 = botReply[reply].replace("<br>", "")
                   text2 = text1.replace("<b>", "")
                   text3 = text2.replace("</b>", "")

                   botReplyId = dbChatlogHandler.messagesChatlog(text3, settings.CONN_STRING)
                   seqNo = responseObject["botRequest"]['info']['seqNo']
                   seqNo = int(seqNo) + 1
                   responseObject["botRequest"]['info']['seqNo'] = seqNo
                   convId = responseObject["botRequest"]['info']['chatLogsConvId']
                   dbChatlogHandler.messagesSequenceChatlogs(convId, seqNo, botReplyId,
                                                            True, settings.CONN_STRING)

        return HttpResponse(json.dumps(outputDict, sort_keys=False, indent=4, separators=(',', ': ')),
                            content_type='application/json', charset='UTF-8')
    else:

        chatlogsConvId = request.GET.get('chatlogsConvId')
        print "chatlogsConvId : ", chatlogsConvId

        outputDict = dbChatlogHandler.chatLogsofConversation(chatlogsConvId, settings.CONN_STRING)

        return HttpResponse(json.dumps(outputDict, sort_keys=False, indent=4, separators=(',', ': ')),
                            content_type='application/json', charset='UTF-8')

        pass


def hello(delay_in_sec):
    print 'deplay in second',processInputRequest.var
    if "phoneNo" in processInputRequest.var.keys():
        print "Phone number hai "
        phoneNumber = processInputRequest.var["phoneNo"]
        name = processInputRequest.var["name"]
        email = str(phoneNumber) + "@greenberet.in"
        if "emailId" not in processInputRequest.var.keys():
            [leadId, message] = addLead(name, email, phoneNumber, '')
            print "email id not in info"

class greenBeretBotOutputProcessorType1(OutputProcessorType1):

    def __init__(self,outputContent=None,botDisplayName="",responseType="json", uiComponentTypeFunctionMap=None):

        uiComponentTypeFunctionMap={

            'Form':greenBeretBotOutputProcessorType1.processForm,
            'Bubble' : greenBeretBotOutputProcessorType1.Bubble,
            #"SimpleMessage":CRMBotOutputProcessorType1.processSimpleMessage
            'UserAccForm': greenBeretBotOutputProcessorType1.processUserAccForm,
            'QueryTextBox': greenBeretBotOutputProcessorType1.processQueryTextBox,
            'FeedbackTextBox':greenBeretBotOutputProcessorType1.processFeedbackTextBox,
            # ------------
            'UserDetailsForm': greenBeretBotOutputProcessorType1.processUserDetailsForm,
            'EmailIdComponent': greenBeretBotOutputProcessorType1.processEmailIdComponent,
            'Feedback':greenBeretBotOutputProcessorType1.processFeedBack,
            "StartContinueComponent": greenBeretBotOutputProcessorType1.processStartContinueComponent,
            "UserNameComponent": greenBeretBotOutputProcessorType1.processUserNameComponent,
            "PhoneNoComponent": greenBeretBotOutputProcessorType1.processsPhoneNoComponent,
            "CommentComponent": greenBeretBotOutputProcessorType1.processCommentComponent,
            "PropertyBrowse": greenBeretBotOutputProcessorType1.processPropertyBrowse,

        }

        super(greenBeretBotOutputProcessorType1, self).__init__(outputContent=outputContent,botDisplayName=botDisplayName,
                                                           responseType=responseType,uiComponentTypeFunctionMap=uiComponentTypeFunctionMap)


    def processForm(self,uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """
        #
        # botObject = [el for el in responseObject.content if isinstance(el, BotBrain)][0]
        #

        #if "BotBrain" in self.outputDict.keys():
            # if self.responseType=="json"
        """

        {
        responseElement=
                    "data":[
                            {"name":"text","required":"True"},
                            {"emailId":"email","required":"True"},
                            {"phoneNo":"numeric","required":"True"},
                            {"address": "text", "required": "False"}

                            ],
                    "componentType":"Form",
                    "message":"",
                    "name":str(uuid.uuid4()),  # a random name everytime
                    "tag":"UserDetails",
                    "nextCallTag": ""

                }
        """
        uiComponent.pop("name",None)
        uiComponent.pop("tag",None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "Form" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["Form"].append(uiComponent)
            else:
                self.outputDict["Form"] = [uiComponent]


#  ----------------
    def Bubble(self,uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """
        #
        # botObject = [el for el in responseObject.content if isinstance(el, BotBrain)][0]
        #

        #if "BotBrain" in self.outputDict.keys():
            # if self.responseType=="json"

        uiComponent.pop("message",None)
        uiComponent.pop("tag",None)
        uiComponent.pop("name", None)
        uiComponent.pop("componentType", None)


        if self.responseType == "json":
            if "Bubble" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["Bubble"].append(uiComponent)
            else:
                self.outputDict["Bubble"] = [uiComponent]

    def processPropertyBrowse(self, uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """
        #
        # botObject = [el for el in responseObject.content if isinstance(el, BotBrain)][0]
        #

        # if "BotBrain" in self.outputDict.keys():
        # if self.responseType=="json"

        uiComponent.pop("message", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("name", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "PropertyBrowse" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["PropertyBrowse"].append(uiComponent)
            else:
                self.outputDict["PropertyBrowse"] = [uiComponent]

    def processUserAccForm(self,uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """
        #
        # botObject = [el for el in responseObject.content if isinstance(el, BotBrain)][0]
        #

        #if "BotBrain" in self.outputDict.keys():
            # if self.responseType=="json"
        """

        {
        responseElement=
                    "data":[
                            {"name":"text","required":"True"},
                            {"emailId":"email","required":"True"},
                            {"phoneNo":"numeric","required":"True"},
                            {"address": "text", "required": "False"}

                            ],
                    "componentType":"Form",
                    "message":"",
                    "name":str(uuid.uuid4()),  # a random name everytime
                    "tag":"UserDetails",
                    "nextCallTag": ""

                }
        """
        uiComponent.pop("name",None)
        uiComponent.pop("tag",None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "UserAccForm" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["UserAccForm"].append(uiComponent)
            else:
                self.outputDict["UserAccForm"] = [uiComponent]

    '''
    def processTextBox(self,uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """
        #
        # botObject = [el for el in responseObject.content if isinstance(el, BotBrain)][0]
        #

        #if "BotBrain" in self.outputDict.keys():
            # if self.responseType=="json"
        """

        {
        responseElement=
                    "data":[
                            {"name":"text","required":"True"},
                           ],
                    "componentType":"TextBox",
                    "message":"",
                    "name":str(uuid.uuid4()),  # a random name everytime
                    "tag":"TextBox",
                    "nextCallTag": ""

                }
        """
        uiComponent.pop("name",None)
        uiComponent.pop("tag",None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "TextBox" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["TextBox"].append(uiComponent)
            else:
                self.outputDict["TextBox"] = [uiComponent]
    '''

# -----------------------------------------------------------------------------------------------------
    def processUserDetailsForm(self,uiComponent):
        """

                This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

                :param outputDict: A dictionary object , similar to the output structure
                :param responseObject: MessageLayerModel object which contains the response to the front end
                :return: modified outputDict (with the bot brain added)

                """
        #
        """

        {
        responseElement=
                    "data":[
                            {"name":"text","emailId":"True"},
                           ],
                    "componentType":"UserDetailsForm",
                    "message":"",
                    "name":str(uuid.uuid4()),  # a random name everytime
                    "tag":"UserDetailsForm",
                    "nextCallTag": "User_Details_Storage"

                }
        """
        uiComponent.pop("name", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("componentType", None)
        if self.responseType == "json":
            if "UserDetailsForm" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["UserDetailsForm"].append(uiComponent)
            else:
                self.outputDict["UserDetailsForm"] = [uiComponent]

    def processEmailIdComponent(self,uiComponent):
        """

                This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

                :param outputDict: A dictionary object , similar to the output structure
                :param responseObject: MessageLayerModel object which contains the response to the front end
                :return: modified outputDict (with the bot brain added)

        """


        uiComponent.pop("name", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "EmailIdComponent" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["EmailIdComponent"].append(uiComponent)
            else:
                self.outputDict["EmailIdComponent"] = [uiComponent]

    def processFeedBack(self,uiComponent):
        """

                This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

                :param outputDict: A dictionary object , similar to the output structure
                :param responseObject: MessageLayerModel object which contains the response to the front end
                :return: modified outputDict (with the bot brain added)

        """


        uiComponent.pop("name", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "FeedBack" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["FeedBack"].append(uiComponent)
            else:
                self.outputDict["FeedBack"] = [uiComponent]


    def processStartContinueComponent(self,uiComponent):
        """

                This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

                :param outputDict: A dictionary object , similar to the output structure
                :param responseObject: MessageLayerModel object which contains the response to the front end
                :return: modified outputDict (with the bot brain added)

        """


        uiComponent.pop("name", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "StartContinueComponent" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["StartContinueComponent"].append(uiComponent)
            else:
                self.outputDict["StartContinueComponent"] = [uiComponent]


    def processUserNameComponent(self,uiComponent):
        """

                This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

                :param outputDict: A dictionary object , similar to the output structure
                :param responseObject: MessageLayerModel object which contains the response to the front end
                :return: modified outputDict (with the bot brain added)

        """


        uiComponent.pop("name", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "UserNameComponent" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["UserNameComponent"].append(uiComponent)
            else:
                self.outputDict["UserNameComponent"] = [uiComponent]


    def processsPhoneNoComponent(self,uiComponent):
        """

                This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

                :param outputDict: A dictionary object , similar to the output structure
                :param responseObject: MessageLayerModel object which contains the response to the front end
                :return: modified outputDict (with the bot brain added)

        """


        uiComponent.pop("name", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "PhoneNoComponent" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["PhoneNoComponent"].append(uiComponent)
            else:
                self.outputDict["PhoneNoComponent"] = [uiComponent]


    def processCommentComponent(self,uiComponent):
        """

                This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

                :param outputDict: A dictionary object , similar to the output structure
                :param responseObject: MessageLayerModel object which contains the response to the front end
                :return: modified outputDict (with the bot brain added)

        """


        uiComponent.pop("name", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "CommentComponent" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["CommentComponent"].append(uiComponent)
            else:
                self.outputDict["CommentComponent"] = [uiComponent]


    def processQueryTextBox(self,uiComponent):
        """

                This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

                :param outputDict: A dictionary object , similar to the output structure
                :param responseObject: MessageLayerModel object which contains the response to the front end
                :return: modified outputDict (with the bot brain added)

        """


        uiComponent.pop("name", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "QueryTextBox" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["QueryTextBox"].append(uiComponent)
            else:
                self.outputDict["QueryTextBox"] = [uiComponent]


    def processFeedbackTextBox(self,uiComponent):
        """

                This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

                :param outputDict: A dictionary object , similar to the output structure
                :param responseObject: MessageLayerModel object which contains the response to the front end
                :return: modified outputDict (with the bot brain added)

        """


        uiComponent.pop("name", None)
        uiComponent.pop("tag", None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "FeedbackTextBox" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["FeedbackTextBox"].append(uiComponent)
            else:
                self.outputDict["FeedbackTextBox"] = [uiComponent]




