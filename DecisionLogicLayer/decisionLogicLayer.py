from __future__ import unicode_literals
from django.db import models

from PaxcelBotFramework.DecisionLogicLayer.models import SolverFactory

import datetime
from django.conf import settings

from PaxcelBotFramework.DataAccessLayer.chatlogsHandler import dbChatlogHandler
import json
from sendEmail import sendingUserDetails

from DataAccessLayer.dbHandler import addLead
# from MessageManager.messageHandler import createMessage
# from PaxcelBotFramework.BotManager.models.context import ContextHelperFunctions

import uuid

import logging
from json2html import *

logger = logging.getLogger(__name__)

# Define global flags for all the flows that you want to enable
flagAsianAdventure = False # global variable


# On() class
class On():
    def __init__(self, watsonOutput=None, operation=None):
        '''

        This is the input object to the Policy file of Intellect solver
        For more details read some examples in

        https://pypi.python.org/pypi/Intellect

        Solver Object Initialiser

        :param topIntent: the top most intent
        :param operation: function handle (modified by the policy file)
        '''

        self.watsonOutput = watsonOutput
        self.operation = operation

    @property
    def watsonOutput(self):
        return self.watsonOutput

    @watsonOutput.setter
    def watsonOutput(self, value):
        self.watsonOutput = value

    @property
    def operation(self):
        return self.operation

    @operation.setter
    def operation(self, value):
        self.operation = value
# End of On class

# Do() class
class Do():
    """
    This contains a list of all the actions based on the decision made by the rule engine
    each function will have a BotResponse() object (for definition see PaxcelBotFramework.Botmanager.models file) as input
    and will return a MessageLayerModel() object (for definition see PaxcelBotFramework.MessageFormationLayer.models)
    the data access layer may be called as required, and the output is sent to the MessageFormationLayer for
    creation of message.
    """

    # Block 1: Handle Greetings
    @staticmethod
    def handleGreetings(botResponse):
        # accessing the global variables

        # initializations
        returnedComponentTray = []
        context = botResponse["context"]
        # botResponse["botRequest"]["info"]['notClear'] = 0

        domain = botResponse["botRequest"]["rawInput"]["data"]["domain"]
        url = botResponse["botRequest"]["rawInput"]["data"]["url"]
        botResponse["botRequest"]["info"]['url'] = url

        botResponse["botRequest"]["info"]['websiteId'] = 1
        websiteId = botResponse["botRequest"]["info"]['websiteId']

        botResponse["botRequest"]["info"]['parentDataId'] = -1
        parentDataId = botResponse["botRequest"]["info"]['parentDataId']

        conversationId = botResponse["botRequest"]["info"]['ConvSessionId']
        startTime = datetime.datetime.now()
        chatlogsId = dbChatlogHandler.userConversationTimeDetails(conversationId, 0, startTime, startTime,
                                                                  settings.CONN_STRING)
        print "chatlogsId : ", chatlogsId

        userMessage = 'url: ' + url
        botResponse["botRequest"]['info']['chatLogsConvId'] = chatlogsId
        botResponse["botRequest"]['info']['seqNo'] = 0
        msgId = dbChatlogHandler.messagesChatlog(userMessage, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        print "seqNo : ", seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)
        # Set the flags as per the domain
        # process the basic and info greetings
        returnedCompGreeting = processGreetings()
        infoField = botResponse["botRequest"]["info"]

        if "name"  in infoField.keys():
            if "mailId" and "prop" not in infoField.keys():
                name = botResponse["botRequest"]['info']["name"]


                subject = str(name) + " is again trying to connect with buddy on website"
                botResponse["botRequest"]['info']['subject'] = subject
                thanksForName = {
                    "data": name,  # from data access layer
                    "tag": "createMessageGreeting.thanksForNameBack",  # this is for message formation layer
                    "componentType": "SimpleMessage",
                    "message": "",  #
                    "name": str(uuid.uuid4())  # a random name everytime
                }
                compGreetings2 = {
                    "data": "",  # from data access layer
                    "tag": "createMessageGreeting.basic",  # this is for message formation layer
                    "componentType": "SimpleMessage",
                    "message": "",  #
                    "name": str(uuid.uuid4())  # a random name everytime
                }
                helpForProperty = {
                    "data": "",  # from data access layer
                    "tag": "createMessageGreeting.helpForProperty",  # this is for message formation layer
                    "componentType": "SimpleMessage",
                    "message": "",  #
                    "name": str(uuid.uuid4())  # a random name everytime
                }



                websiteId = botResponse["botRequest"]["info"]['websiteId']

                parentDataId = botResponse["botRequest"]["info"]['parentDataId']

                compCategoryList = {
                    "parentId": parentDataId,
                    "websiteId": websiteId,
                    "data": "",  # from data access layer
                    "tag": "createBotData.data",  # this is for message formation layer
                    "componentType": "Bubble",
                    "message": "",  #
                    "name": str(uuid.uuid4())  # a random name everytime
                }



                returnedComponentTray.append(thanksForName)
                #returnedComponentTray.append(compGreetings2)
                returnedComponentTray.append(helpForProperty)
                returnedComponentTray.append(compCategoryList)
        else:
            askForName = {
                "data": "",  # from data access layer
                "tag": "createMessageGreeting.askName",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }

            returnedComponentTray.append(askForName)

            comp2 = {

                "componentType": "UserNameComponent",
                "message": "",
                "name": str(uuid.uuid4()),  # a random name everytime
                "tag": "UserName",
                "nextCallTag": "Save_User_Name"

            }
            returnedComponentTray.append(comp2)
            returnedComponentTray = returnedCompGreeting + returnedComponentTray
        return [botResponse, returnedComponentTray]


    @staticmethod
    def saveUserName(botResponse):
        componentTray = []
        context = botResponse["context"]
        name = botResponse["botRequest"]["rawInput"]["data"]["name"]
        botResponse["botRequest"]['info']["name"] = name

        url = botResponse["botRequest"]["info"]['url']

        userMessage = 'name: ' + name
        subject = "New lead on your website named: " + str(name)
        botResponse["botRequest"]['info']['subject'] = subject
        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']
        msgId = dbChatlogHandler.messagesChatlog(userMessage, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)

        thanksForName = {
            "data": name,  # from data access layer
            "tag": "createMessageGreeting.thanksForName",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        componentTray.append(thanksForName)

        helpForProperty = {
            "data": "",  # from data access layer
            "tag": "createMessageGreeting.helpForProperty",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        componentTray.append(helpForProperty)

        websiteId = botResponse["botRequest"]["info"]['websiteId']

        parentDataId = botResponse["botRequest"]["info"]['parentDataId']

        compCategoryList = {
            "parentId": parentDataId,
            "websiteId": websiteId,
            "data": "",  # from data access layer
            "tag": "createBotData.data",  # this is for message formation layer
            "componentType": "Bubble",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }
        componentTray.append(compCategoryList)
        return [botResponse, componentTray]


    @staticmethod
    def saveUserDetails(botResponse):
        componentTray = []
        context = botResponse["context"]
        name = botResponse["botRequest"]['info']["name"]
        emailId = botResponse["botRequest"]["rawInput"]["data"]["emailId"]
        phoneNumber = botResponse["botRequest"]["rawInput"]["data"]["phoneNumber"]
        comments = botResponse["botRequest"]["rawInput"]["data"]["addComments"]

        botResponse["botRequest"]['info']["emailId"]= emailId
        botResponse["botRequest"]['info']["phoneNumber"]= phoneNumber
        botResponse["botRequest"]['info']["comments"]= comments

        url = botResponse["botRequest"]["info"]['url']

        userMessage = 'name: ' + name + '; emailId: ' + emailId + '; phoneNumber: ' + phoneNumber + '; comments: ' + str(
            comments)

        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']
        msgId = dbChatlogHandler.messagesChatlog(userMessage, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)
        websiteUrl = botResponse["botRequest"]["info"]['url']

        leadId = addLead(name, emailId, phoneNumber)
        botResponse["botRequest"]['info']['leadId']= leadId
        print "****** leadId : " , leadId

        chatLogsConvId = botResponse["botRequest"]['info']['chatLogsConvId']
        dbChatlogHandler.updateUserIdBasedOnchatLogsConvId(leadId, chatLogsConvId, settings.CONN_STRING)
        logsArr = dbChatlogHandler.chatLogs(chatLogsConvId, leadId, settings.CONN_STRING)
        for var in range(len(logsArr)):
            tupleObj = logsArr[var]

        for t in range(len(tupleObj)):
            object2 = tupleObj[t]

        json_obj_in_html = ''

        for t in range(len(object2)):
            # object3 = object2[t]['message']
            if object2[t]['isbot'] == True:
                message = object2[t]['message']
                d = {}
                d['Bheem'] = message

                json_obj_in_html += """\

                                            <div style="margin-left: 30px"> 
                                                <div style="display: flex; margin-bottom: 15px">
                                                <div style="font-weight: bold; min-width: 80px; ">Beret</div><div style="">{message}</div>

                                            </div></div>
                                        """.format(message=message)

                # json_obj_in_html += json2html.convert(json=d)
            else:
                message = object2[t]['message']
                d = {}
                d['User'] = message

                json_obj_in_html += """\
                                            <div style="margin-left: 30px"> 
                                                <div style="display: flex;margin-bottom: 15px">
                                                <div style="font-weight: bold; min-width: 80px; ">User</div><div style="">{message}</div>

                                            </div></div>
                                                        """.format(message=message)

                # json_obj_in_html += json2html.convert(json=d)

        sendingUserDetails(name, emailId, phoneNumber, url, comments, json_obj_in_html,'')

        thanksForDetails = {
            "data": name,  # from data access layer
            "tag": "createMessageGreeting.thanksForDetails",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        componentTray.append(thanksForDetails)
        return [botResponse, componentTray]

    @staticmethod
    def handleBubbleData(botResponse):
        componentTray = []
        context = botResponse["context"]

        Id = botResponse["botRequest"]["rawInput"]["data"]["optionId"]
        optionName = botResponse["botRequest"]["rawInput"]["data"]["optionName"]
        node_type_id = botResponse["botRequest"]["rawInput"]["data"]["node_type_id"]
        # optionData = botResponse["botRequest"]["rawInput"]["data"]["optionData"]
        websiteId = botResponse["botRequest"]["info"]['websiteId']

        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']

        print "****** Id :", Id
        print "******* optionName : ", optionName
        print "******* website Id : ", websiteId

        msgId = dbChatlogHandler.messagesChatlog(optionName, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1

        print seqNo, 'seqno test'
        botResponse["botRequest"]['info']['node_type_id'] = node_type_id
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False, settings.CONN_STRING)
        print "+++++261+++++parent Id : ", Id
        compCategoryList2 = {
            "parentId": Id,
            "websiteId": websiteId,
            "data": "",  # from data access layer
            "tag": "createBotData.data",  # this is for message formation layer
            "componentType": "Bubble",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }



        if node_type_id == 1 and optionName == "Personal Banking":
            botResponse["botRequest"]["info"]['rootName'] = botResponse["botRequest"]["rawInput"]["data"]["optionName"]
            print "++++++++++optionName : ", optionName

            askForBudget = {
                "data": "",  # from data access layer
                "tag": "createMessageAbout.askForCallArrange",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }

            componentTray.append(askForBudget)
            componentTray.append(compCategoryList2)
        if node_type_id == 1 and optionName != "Personal Banking":
           componentTray.append(compCategoryList2)
        if node_type_id == 2 and optionName == "Cards":
          botResponse["botRequest"]["info"]['rootName'] = botResponse["botRequest"]["rawInput"]["data"]["optionName"]
          print "++++++++++optionName : ", optionName

          askForBudget = {
            "data": "",  # from data access layer
            "tag": "createMessageAbout.askForCard",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
          }

          componentTray.append(askForBudget)
          componentTray.append(compCategoryList2)
        if node_type_id == 2 and optionName != "Cards":
          componentTray.append(compCategoryList2)
        if node_type_id == 4 and optionName == "VISA SIGNATURE DEBIT CARD":
           botResponse["botRequest"]["info"]['rootName'] = botResponse["botRequest"]["rawInput"]["data"]["optionName"]
           print "++++++++++optionName : ", optionName
          # askForBudget = {
          #   "data": "",  # from data access layer
          #   "tag": "createMessageAbout.askForCard",  # this is for message formation layer
          #   "componentType": "SimpleMessage",
          #   "message": "",  #
          #   "name": str(uuid.uuid4())  # a random name everytime
          # }
          #
          # componentTray.append(askForBudget)
          #componentTray.append(compCategoryList2)

           propertyBrowseComponent = {
             "parentId": Id,
             "websiteId": websiteId,
             "data": "",
             "componentType": "PropertyBrowse",
             "message": "",
             "name": str(uuid.uuid4()),  # a random name everytime
             "tag": "PropertyBrowse.data",
             "nextCallTag": "Save_User_Comment"

           }
           componentTray.append(propertyBrowseComponent)
        if node_type_id == 4 and optionName != "VISA SIGNATURE DEBIT CARD":
           componentTray.append(compCategoryList2)
        if node_type_id == 12:
           propertyBrowseComponent = {

            "data": "",
            "componentType": "UserDetailsForm",
            "message": "",
            "name": str(uuid.uuid4()),  # a random name everytime
            "tag": "UserDetails",
            "nextCallTag": ""

          }
           componentTray.append(propertyBrowseComponent)

        return [botResponse, componentTray]

    @staticmethod
    def saveUserEmailId(botResponse):
        componentTray = []
        context = botResponse["context"]
        emailId = botResponse["botRequest"]["rawInput"]["data"]["emailId"]
        botResponse["botRequest"]['info']["mailId"] = emailId
        botResponse["botRequest"]['info']["emailId"] = emailId
        name = botResponse["botRequest"]["info"]['name']

        userMessage = 'emailId: ' + emailId

        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']
        msgId = dbChatlogHandler.messagesChatlog(userMessage, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)
        phoneNo = botResponse["botRequest"]['info']['phoneNo']
        websiteUrl = botResponse["botRequest"]['info']['url']
        repEmail = botResponse["botRequest"]['info']["repEmail"]

        [vid, messageText] = getVidByEmail(repEmail)
        vidByEmail = [vid, messageText]
        vid = vidByEmail[0]
        messageText = vidByEmail[1]
        if messageText == "email exist":
            botResponse["botRequest"]['info']['leadId'] =vid
            updateLeadByVid(vid,emailId)
            thanksForEmailId = {
                "data": "",  # from data access layer
                "tag": "createMessageGreeting.thanksForEmailId",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            componentTray.append(thanksForEmailId)
            # prop = botResponse["botRequest"]["info"]['prop']
            # budget = botResponse["botRequest"]["info"]['budget']
            # location = botResponse["botRequest"]["info"]['location']
            # from DataAccessLayer.dbHandler import updateLead
            # name = botResponse["botRequest"]['info']['name']
            # message = name + " is interested in " + prop + " at " + location + " in " + budget + " budget"
            # leadId = botResponse["botRequest"]['info']['leadId']
            # updateLead(leadId, message)
        else:
            [leadId, message] = addLead(name, emailId, phoneNo, websiteUrl)
            apioutput = [leadId, message]
            leadId = apioutput[0]
            message = apioutput[1]
            botResponse["botRequest"]['info']['leadId'] = leadId
            if message == "already exist":
                welcomeBackMessage = {
                    "data": "",  # from data access layer
                    "tag": "createMessageGreeting.welcomeBackMessageAgain",  # this is for message formation layer
                    "componentType": "SimpleMessage",
                    "message": "",  #
                    "name": str(uuid.uuid4())  # a random name everytime
                }

                componentTray.append(welcomeBackMessage)
                name = botResponse["botRequest"]['info']['name']
                subject = str(name) + " is again trying to connect with buddy on website"
                botResponse["botRequest"]['info']['subject'] = subject
                from DataAccessLayer.dbHandler import updateLead

                message = name + " is again trying to connect with buddy on website"
                leadId = botResponse["botRequest"]['info']['leadId']
                updateLead(leadId, message)

            else:
                thanksForEmailId = {
                    "data": "",  # from data access layer
                    "tag": "createMessageGreeting.thanksForEmailId",  # this is for message formation layer
                    "componentType": "SimpleMessage",
                    "message": "",  #
                    "name": str(uuid.uuid4())  # a random name everytime
                }
                subject = botResponse["botRequest"]['info']['subject']
                from DataAccessLayer.dbHandler import updateLead

                leadId = botResponse["botRequest"]['info']['leadId']
                updateLead(leadId, subject)
                componentTray.append(thanksForEmailId)
                # prop = botResponse["botRequest"]["info"]['prop']
                # budget =botResponse["botRequest"]["info"]['budget']
                # location =botResponse["botRequest"]["info"]['location']
                # from DataAccessLayer.dbHandler import updateLead
                # name = botResponse["botRequest"]['info']['name']
                # message = name + " is interested in " + prop + " at " + location + " in " + budget + " budget"
                # leadId = botResponse["botRequest"]['info']['leadId']
                # updateLead(leadId, message)


        # askForComment = {
        #     "data": "",  # from data access layer
        #     "tag": "createMessageGreeting.askForComment",  # this is for message formation layer
        #     "componentType": "SimpleMessage",
        #     "message": "",  #
        #     "name": str(uuid.uuid4())  # a random name everytime
        # }
        #
        # componentTray.append(askForComment)
        # commentComponent = {
        #
        #     "componentType": "CommentComponent",
        #     "message": "",
        #     "name": str(uuid.uuid4()),  # a random name everytime
        #     "tag": "UserComment",
        #     "nextCallTag": "Save_User_Comment"
        #
        # }
        # componentTray.append(commentComponent)
        '''
        
        from DataAccessLayer.propertyDealsFromApi import propertyDeals
        result = propertyDeals
        print "+++++result+++++++ : ", result
        '''
        compBrowseProp = {

            "data": "",  # from data access layer
            "tag": "createBotData.browseProp",  # his is for message formation layer
            "componentType": "Bubble",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }
        componentTray.append(compBrowseProp)
        return [botResponse, componentTray]


    @staticmethod
    def saveUserPhoneNo(botResponse):
        componentTray = []
        context = botResponse["context"]
        phoneNo = botResponse["botRequest"]["rawInput"]["data"]["phoneNo"]
        botResponse["botRequest"]['info']["phoneNo"] = phoneNo
        botResponse["botRequest"]['info']["repEmail"]=botResponse["botRequest"]["rawInput"]["data"]["repEmail"]
        userMessage = 'phoneNo: ' + phoneNo

        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']
        msgId = dbChatlogHandler.messagesChatlog(userMessage, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)
        name =botResponse["botRequest"]['info']["name"]
        email = str(phoneNo) + "@greenberet.in"
        botResponse["botRequest"]['info']["emailId"] = email
        [leadId, message] = addLead(name, email, phoneNo, '')
        leadArr =[leadId, message]
        leadId = leadArr[0]
        numberWhatsapp = "91" + str(phoneNo)

        prop = botResponse["botRequest"]["info"]['prop']
        budget = botResponse["botRequest"]["info"]['budget']
        infoField = botResponse["botRequest"]["info"]

        if "location" in infoField.keys():
           location = botResponse["botRequest"]["info"]['location']
        else:
             location = ''
        InsertUserDetailsWeb(leadId, numberWhatsapp, email, prop, location, budget)
        from DataAccessLayer.dbHandler import updateLead
        name = botResponse["botRequest"]['info']['name']
        message = name + " is interested in " + prop + " property at " + location + " with a budget of Rs " + budget

        updateLead(leadId, message)
        thanksForPhoneNo = {
            "data": "",  # from data access layer
            "tag": "createMessageGreeting.thanksForPhoneNo",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        componentTray.append(thanksForPhoneNo)

        askForEmailId = {
            "data": "",  # from data access layer
            "tag": "createMessageGreeting.askForEmailId",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        componentTray.append(askForEmailId)

        comp2 = {

            "componentType": "EmailIdComponent",
            "message": "",
            "name": str(uuid.uuid4()),  # a random name everytime
            "tag": "UserEmailId",
            "nextCallTag": "Save_User_EmailId"

        }
        componentTray.append(comp2)

        return [botResponse, componentTray]

    @staticmethod
    def saveUserComment(botResponse):
        componentTray = []
        context = botResponse["context"]
        comments = botResponse["botRequest"]["rawInput"]["data"]["comments"]
        botResponse["botRequest"]['info']["comments"] = comments

        userMessage = 'comments: ' + comments

        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']
        msgId = dbChatlogHandler.messagesChatlog(userMessage, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)

        leadId = botResponse["botRequest"]['info']['leadId']
        print "****** leadId : ", leadId
        name = botResponse["botRequest"]['info']['name']
        emailId = botResponse["botRequest"]['info']['emailId']
        phoneNo = botResponse["botRequest"]['info']['phoneNo']
        comments = botResponse["botRequest"]['info']['comments']
        url = botResponse["botRequest"]['info']['url']

        chatLogsConvId = botResponse["botRequest"]['info']['chatLogsConvId']
        dbChatlogHandler.updateUserIdBasedOnchatLogsConvId(leadId, chatLogsConvId, settings.CONN_STRING)
        logsArr = dbChatlogHandler.chatLogs(chatLogsConvId, leadId, settings.CONN_STRING)
        for var in range(len(logsArr)):
            tupleObj = logsArr[var]

        for t in range(len(tupleObj)):
            object2 = tupleObj[t]

        json_obj_in_html = ''
        messageList =[]
        for t in range(len(object2)):
            # object3 = object2[t]['message']
            if object2[t]['isbot'] == True:
                message = object2[t]['message']
                messageList.append(str(message))
                d = {}
                d['Beret'] = message

                json_obj_in_html += """\

                                                    <div style="margin-left: 30px"> 
                                                        <div style="display: flex; margin-bottom: 15px">
                                                        <div style="font-weight: bold; min-width: 80px; ">Beret</div><div style="">{message}</div>

                                                    </div></div>
                                                """.format(message=message)

                # json_obj_in_html += json2html.convert(json=d)
            else:
                message = object2[t]['message']
                d = {}
                messageList.append(str(message))
                d['User'] = message

                json_obj_in_html += """\
                                                    <div style="margin-left: 30px"> 
                                                        <div style="display: flex;margin-bottom: 15px">
                                                        <div style="font-weight: bold; min-width: 80px; ">User</div><div style="">{message}</div>

                                                    </div></div>
                                                                """.format(message=message)

                # json_obj_in_html += json2html.convert(json=d)
        subject = botResponse["botRequest"]['info']['subject']
        sendingUserDetails(name, emailId, phoneNo, url, comments, json_obj_in_html, subject)
        from DataAccessLayer.dbHandler import updateLead
        name = botResponse["botRequest"]['info']['name']
        leadId = botResponse["botRequest"]['info']['leadId']
        message = name +" commented:" +comments

        print messageList
        updateLead(leadId, message)



        thanksForComments = {
            "data": "",  # from data access layer
            "tag": "createMessageGreeting.thanksForComments",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        componentTray.append(thanksForComments)

        return [botResponse, componentTray]




# processGreetings Function is responsible for fetching the greeting messages

def processGreetings():
    componentTray = []
    compGreetings1 = {
        "data": "",  # from data access layer
        "tag": "createMessageGreeting.welcome",  # this is for message formation layer
        "componentType": "SimpleMessage",
        "message": "",  #
        "name": str(uuid.uuid4())  # a random name everytime
    }

    # compGreetings2 = {
    #      "data": "",  # from data access layer
    #     "tag": "createMessageGreeting.basic",  # this is for message formation layer
    #     "componentType": "SimpleMessage",
    #      "message": "",  #
    #      "name": str(uuid.uuid4())  # a random name everytime
    #  }

    componentTray.append(compGreetings1)
    # componentTray.append(compGreetings2)
    # componentTray.append(compGreetings3)

    return componentTray

# processListFlow function is responsible for fetching the bubbles data

def processListFlow(websiteId,parentDataId):
    componentTray = []

    compCategoryList2 = {
        "parentId":parentDataId,
        "websiteId":websiteId,
        "data": "",  # from data access layer
        "tag": "createBotData.data",  # this is for message formation layer
        "componentType": "Bubble",
        "message": "",  #
        "name": str(uuid.uuid4())  # a random name everytime
    }
    componentTray.append(compCategoryList2)
    return componentTray


def processBotResponse(botManagerResponse):

    """

    Loads the Policy solver and performs policy solution expressions , returns messageLayerResponse object

    :param:  botResponse : A BotResponse() object (for definition see PaxcelBotFramework.Botmanager.models file)
    :return: messageLayerResponse : A MessageLayerModel() object (for definition see PaxcelBotFramework.MessageFormationLayer.models)


    """

    policyFilePath = settings.POLICY_FILE_PATH
    policySolver =SolverFactory().getSolverInstance(type=settings.SOLVER_TYPE,policyFilePath=policyFilePath)
    watsonOutput = botManagerResponse["outputMessages"][-1]["message"]

    print "******** watson output",watsonOutput
    logger.info(
        "User selection: {0}".format((watsonOutput))
    )
    OnObjectUpdated = policySolver.getResponse(On(watsonOutput=watsonOutput)) #on object the operation property has been updated
    [botResponse, componentTray]=OnObjectUpdated.operation(botManagerResponse)


    uiComponentTrayFromDecisionLayer = {
        "botResponse": botResponse,
        "uiComponents": componentTray

    }

    return uiComponentTrayFromDecisionLayer
