# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings

from botTemplates import TemplateBotData as botTemplates
import logging

from PaxcelBotFramework.BotManager.templateBot import TemplateBot
logger = logging.getLevelName(__name__)


# Create your models here.
def getTemplateBotResponse(botRequest):
        """

        :param botRequest:
        :return:
        """
        templateBot = TemplateBot(botTemplate=botTemplates[settings.TEMPLATE_BOT])
        botResponse = templateBot.getResponse(botRequest)
        return botResponse