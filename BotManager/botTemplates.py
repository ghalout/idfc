TemplateBotData = {

    "simple": {
        "General_Greetings":{

            "outputMessages": [
                    {
                        "raw": {
                        },
                        "message": "General_Greetings",
                        "type": "output"
                    }
                ]
        },
        "Save_User_Name": {

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "Save_User_Name",
                    "type": "output"
                }
            ]

        },



        "Save_User_Details": {

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "Save_User_Details",
                    "type": "output"
                }
            ]

        },

        "Thumbs_Up": {

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "Thumbs_Up",
                    "type": "output"
                }
            ]

        },

        "Thumbs_Down": {

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "Thumbs_Down",
                    "type": "output"
                }
            ]

        },

        "Save_UserFeedback": {

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "Save_UserFeedback",
                    "type": "output"
                }
            ]

        },

        "QueryTextBox": {

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "QueryTextBox",
                    "type": "output"
                }
            ]

        },

        "EmailId_Entered": {

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "EmailId_Entered",
                    "type": "output"
                }
            ]

        },

        "Handle_Bubble_Data": {

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "Handle_Bubble_Data",
                    "type": "output"
                }
            ]

        },

        "Save_User_EmailId":{

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "Save_User_EmailId",
                    "type": "output"
                }
            ]

        },
        "Save_User_PhoneNo":{

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "Save_User_PhoneNo",
                    "type": "output"
                }
            ]

        },
        "Save_User_Comment":{

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "Save_User_Comment",
                    "type": "output"
                }
            ]

        },

        "Store_UserDesignationAndDomain": {

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "Store_UserDesignationAndDomain",
                    "type": "output"
                }
            ]

        },


        "FeedbackCount_Storage":{

            "outputMessages": [
                {
                    "raw": {
                    },
                    "message": "FeedbackCount_Storage",
                    "type": "output"
                }
            ]

        },
    }







}