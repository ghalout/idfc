MessageTemplates = {

    "simple": {
        "createMessageGreeting":{
            "welcome": [
                "Hi, Welcome to <b>Idfc </b>! ",
                "Hey, Welcome to <b>Idfc  </b>! ",
                "Hello, Welcome to <b>Idfc </b>! "
            ],
            "basic": [
                #"I am here to help you in providing the best properly in terms of good investment and end usage. " ,
                "I am here to assist you with all your investment and property needs."
            ],
            "askName": [

                "To assist you further please help us with your name"

             ],
             "welcomeBackMessageAgain": [

                "Thanks for showing trust and coming back to us."

             ],
            "thanksForName":[
                "Nice to meet you, <b>{0} </b>.",
                "You have a lovely name, {0}. "
            ],
            "thanksForNameBack": [

                "Hi {0}, Nice to see you back "
            ],
            "welcomebackmessage": [

                "Are you still looking for {0} properties?  "
            ],
            "helpForProperty": [

                #"What kind of property are you looking for?"
                "Select the service you are looking for today?"
            ],
            "askForEmailId":[
                "Please share your email id."

            ],
            "thanksForEmailId":[
                "Thank you for sharing your email id."
            ],
            "askForPhoneNo":[
                "Our expert will need your phone number in order to get in touch with you.",


            ],
            "thanksForPhoneNo":[
                "Thanks for sharing your contact number."
            ],
            "askForComment":[
                "Help us with what is on your mind"
            ],
            "thanksForComments":[
                #"Thanks for your comments. "
                "Your query has been noted, our buddy will call you back in 24 hrs. "

            ],



            "userDetails": [
                # "I will be more happy to know your details. ",
                "Kindly provide your details so that I can serve you better. ",
                "Kindly enter your details below so that I can serve you better. ",
                "Please enter your details below so that I can serve you better. "

             ],
            "thanksForDetails": [
                "Thank you {0} for sharing your details. ",
                # "Nice to meet you, <b>{0} </b>."
            ],
            "help": [
                "How can I help you?",
                "What are you looking for? ",
                "Let me know how can I help you?",
            ],


},
        "createMessageAbout": {
            "askForPreferredLocation": [
               "Which location are you looking for ?",
                "What is your preferred location ?",
                "Select the location you are interested ?"
                    ],
            "askForCallBack":[
                "Did you get a callback from our buddy ?"
            ],
            "askForhelp": [
                "Please suggest what else I can do for you?"
            ],
            "askForCallArrange": [
                "Please select the service"
            ],
            "askForCard":[
                "Please select the Card.",
                "Kindly select the Card.",

            ],
            "thanksForPreferences":[
                "Well, we have listed down all your requirements and we will connect you shortly. ",
                "Well, we have listed down all your requirements, one of our executives will contact you shortly. ",

            ],
            "replyForThumbsUp":[
                "Thanks for liking us! Leave a comment to us. "
            ],
            "replyForThumbsDown":[
                "Sorry for the inconvenience, please write your suggestions to us. ",
                "Sorry for the inconvenience, please leave a comment to us. "

            ],
            "startOrContinue":[
                "Would you like to start again or continue? "
            ],
            "thanksForQuery":[
                "Thanks for writing to us."
            ],
            "executiveForQuery":[
                "We have listed down your query, one of our executives will contact you shortly."
            ],
            "thanksForFeedback":[
                "Thank you for writing to us.",
                "Thanks for your comment."
            ],


        },
        "createMessageAnythingElse":{
            "basic": [
                "Would you like anything else?",
            ],
        },
    }
}
