# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# from django.db import models
from django.conf import settings

from DataAccessLayer.dbHandler import idfcBotData
# from DataAccessLayer.bubbleData import *

import random

# import random
# Create your models here.

from SimpleMessageTemplates import MessageTemplates as SimpleMessageTemplates
SimpleMessageTemplates = SimpleMessageTemplates[settings.BOT_PERSONA]


# Different type of components/inputs are defined here
def createMessage(uiComponentTrayFromDecisionLayer):
    updatedComponents = []

    componentProcessMap = {
        "SimpleMessage": processSimpleMessage,
        "Bubble": processBubble,
        "UserDetailsForm": processUserDetailsForm,
        "EmailIdComponent": processEmailIdComponent,
        "UserNameComponent": processUserNameComponent,
        "PhoneNoComponent": processPhoneNoComponent,
        "CommentComponent": processCommentComponent,
        "QueryTextBox": processQueryTextBox,
        "FeedbackTextBox": processFeedbackTextBox,

        # "TextAreaHandler": processTextAreaHandler,
        "Feedback": processFeedback,
        "StartContinueComponent" : processStartContinueComponent,
        "PropertyBrowse":processPropertyBrowse,
        # "UserAccForm": processUserAccForm,
        # "TextBox": processTextBox,

    }
    for component in uiComponentTrayFromDecisionLayer["uiComponents"]:
        #
        # based on component type select the specific processing function from component process map and
        # update the component
        #
        componentType = component["componentType"]
        updatedComponent = componentProcessMap[componentType](component)

        # append the updated components

        updatedComponents.append(updatedComponent)

    uiComponentTrayFromMessageLayer = {}
    uiComponentTrayFromMessageLayer["uiComponents"] = updatedComponents
    uiComponentTrayFromMessageLayer["botResponse"] = uiComponentTrayFromDecisionLayer["botResponse"]

    return uiComponentTrayFromMessageLayer


# Simple messages are processed here
def processSimpleMessage(simpleMessageComponent):
    if simpleMessageComponent["tag"] == "createMessageGreeting.welcome":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["welcome"])
    if simpleMessageComponent["tag"] == "createMessageGreeting.helpForProperty":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["helpForProperty"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.basic":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["basic"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.welcomeBackMessageAgain":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["welcomeBackMessageAgain"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.welcomebackmessage":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["welcomebackmessage"]).format(simpleMessageComponent["data"])

    elif simpleMessageComponent["tag"] == "createMessageGreeting.userDetails":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["userDetails"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.thanksForDetails":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["thanksForDetails"]).format(simpleMessageComponent["data"])

    elif simpleMessageComponent["tag"] == "createMessageGreeting.thanksForName":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["thanksForName"]).format(simpleMessageComponent["data"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.thanksForNameBack":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["thanksForNameBack"]).format(simpleMessageComponent["data"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.askName":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["askName"])

    elif simpleMessageComponent["tag"] == "createMessageGreeting.askName":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["askName"])

    elif simpleMessageComponent["tag"] == "createMessageGreeting.askForEmailId":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["askForEmailId"])

    elif simpleMessageComponent["tag"] == "createMessageGreeting.thanksForEmailId":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["thanksForEmailId"])

    elif simpleMessageComponent["tag"] == "createMessageGreeting.askForPhoneNo":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["askForPhoneNo"])

    elif simpleMessageComponent["tag"] == "createMessageGreeting.thanksForPhoneNo":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["thanksForPhoneNo"])

    elif simpleMessageComponent["tag"] == "createMessageGreeting.askForComment":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["askForComment"])


    elif simpleMessageComponent["tag"] ==  "createMessageGreeting.thanksForComments":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["thanksForComments"])


    elif simpleMessageComponent["tag"] == "createMessageAbout.askForPreferredLocation":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageAbout"]["askForPreferredLocation"])
    elif simpleMessageComponent["tag"] == "createMessageAbout.askForCallBack":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageAbout"]["askForCallBack"])
    elif simpleMessageComponent["tag"] == "createMessageAbout.askForhelp":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageAbout"]["askForhelp"])
    elif simpleMessageComponent["tag"] == "createMessageAbout.askForCallArrange":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageAbout"]["askForCallArrange"])



    elif simpleMessageComponent["tag"] == "createMessageAbout.askForCard":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageAbout"]["askForCard"])

    elif simpleMessageComponent["tag"] == "createMessageAbout.thanksForPreferences":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageAbout"]["thanksForPreferences"])

    elif simpleMessageComponent["tag"] == "createMessageAbout.replyForThumbsUp":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageAbout"]["replyForThumbsUp"])
    elif simpleMessageComponent["tag"] == "createMessageAbout.replyForThumbsDown":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageAbout"]["replyForThumbsDown"])

    elif simpleMessageComponent["tag"] == "createMessageAbout.startOrContinue":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageAbout"]["startOrContinue"])

    elif simpleMessageComponent["tag"] == "createMessageAbout.thanksForQuery":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageAbout"]["thanksForQuery"])

    elif simpleMessageComponent["tag"] == "createMessageAbout.executiveForQuery":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageAbout"]["executiveForQuery"])

    elif simpleMessageComponent["tag"] == "createMessageAbout.thanksForFeedback":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageAbout"]["thanksForFeedback"])

    return simpleMessageComponent


# UserForm is processed
def processUserDetailsForm(UserFormComponent):
    if UserFormComponent["tag"] == "UserDetails":
        UserFormComponent["nextCallTag"] = "Save_User_Details"
        UserFormComponent["message"] = ""
    return UserFormComponent


# EmailIdComponent
def processEmailIdComponent(EmailIdComponent):
    if EmailIdComponent["tag"] == "EmailId":
        EmailIdComponent["nextCallTag"] = "Save_User_EmailId"
        EmailIdComponent["message"] = ""
    return EmailIdComponent

def processPropertyBrowse(PropertyBrowse):
    if PropertyBrowse["tag"] == "PropertyBrowse.data":
       print 'bheem gahlout'
       PropertyBrowse["data"] = idfcBotData(PropertyBrowse["websiteId"], PropertyBrowse["parentId"])
       PropertyBrowse["nextCallTag"] = "PropertyBrowse"
    return PropertyBrowse
def processBubble(Bubble):
    if Bubble["tag"] == "createBotData.data":
        print Bubble["data"],'test test test'
        Bubble["data"] = idfcBotData(Bubble["websiteId"], Bubble["parentId"])
        Bubble["nextCallTag"] = "Handle_Bubble_Data"

    return Bubble


def processFeedback(Feedback):
    if Feedback["tag"] == "Feedback":
        print Feedback["data"], 'test test test'
        Feedback["nextCallTag"] = "FeedbackCount_Storage"
    return Feedback


def processStartContinueComponent(StartContinueComponent):
    if StartContinueComponent["tag"] == "StartContinueComponent":
        print StartContinueComponent["data"], 'test test test'
        StartContinueComponent["data"]= [{u'optionName': u'Start', u'optionId': 1},{u'optionName': u'Continue', u'optionId': 2}]
    return StartContinueComponent


def processUserNameComponent(UserNameComponent):
    if UserNameComponent["tag"] == "UserName":
        UserNameComponent["nextCallTag"] = "Save_User_Name"
        UserNameComponent["message"] = ""
    return UserNameComponent


def processPhoneNoComponent(PhoneNoComponent):
    if PhoneNoComponent["tag"] == "UserPhoneNo":
        PhoneNoComponent["nextCallTag"] = "Save_User_PhoneNo"
        PhoneNoComponent["message"] = ""
    return PhoneNoComponent


def processCommentComponent(CommentComponent):
    if CommentComponent["tag"] == "UserComment":
        CommentComponent["nextCallTag"] = "Save_User_Comment"
        CommentComponent["message"] = ""
    return CommentComponent


def processQueryTextBox(QueryTextBox):
    if QueryTextBox["tag"] == "QueryTextBox":
        # print QueryTextBox["data"], 'test test test'
        QueryTextBox["type"] = "QueryTextBox"
        QueryTextBox["displayText"] = SimpleMessageTemplates["createMessageAbout"]["queryTextBox"]
    return QueryTextBox


def processFeedbackTextBox(FeedbackTextBox):
    if FeedbackTextBox["tag"] == "FeedbackTextBox":
        # print QueryTextBox["data"], 'test test test'
        FeedbackTextBox["type"] = "FeedbackTextBox"
        FeedbackTextBox["nextCallTag"] = "Save_UserFeedback"
        # FeedbackTextBox["displayText"] = SimpleMessageTemplates["createMessageAbout"]["feedbackTextBox"]
    return FeedbackTextBox





