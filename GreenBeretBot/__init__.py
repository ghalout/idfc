import inspect
import json
import logging.config
import os

try:
    #from django.conf import settings
    import settings
    if settings.LOG_CONFIG:
        path = settings.LOG_CONFIG
    else:
        path = os.path.join(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))), 'logging.json')

    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        if settings.INFO_LOG_PATH:
            config['handlers']['info_file_handler']['filename'] = os.path.join(settings.INFO_LOG_PATH, "info.log")
        if settings.DEBUG_LOG_PATH:
            config['handlers']['debug_file_handler']['filename'] = os.path.join(settings.DEBUG_LOG_PATH, "debug.log")
        if settings.ERROR_LOG_PATH:
            config['handlers']['error_file_handler']['filename'] = os.path.join(settings.ERROR_LOG_PATH, "error.log")
        if settings.LOG_LEVEL:
            default_level = 'logging.' + settings.LOG_LEVEL
            config['root']['level'] = settings.LOG_LEVEL
        else:
            default_level = 'logging.' + config['root']['level']
        assert isinstance(config, object)
        logging.config.dictConfig(config)
        print("level of logging: {0}".format(default_level))
        logging.basicConfig(level=default_level)
        logger = logging.getLogger(__name__)
        logger.debug("Framework Initialized")

    else:
        print("Logging configuration not found at any path")
        logging.error("Logging configuration not found at any path")

except ImportError:
    print("Django settings not initialized")
    raise
